FROM golang:1.17-alpine

RUN apk add git build-base
WORKDIR /go/src/gitlab.com/Velnbur/blober
COPY . .

# This variable is used to link db's from different containers
# ENV TEST_DB_ADDR=database

ENTRYPOINT ["go", "test", "./..."]
