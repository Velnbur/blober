package postgres

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/distributed_lab/kit/pgdb"
)

const blobsTable = "blobs"

type blobQ struct {
	db   *pgdb.DB
	stmt sq.SelectBuilder
}

func NewBlobRepo(db *pgdb.DB) data.BlobRepo {
	return &blobQ{
		db:   db.Clone(),
		stmt: sq.Select("*").From(blobsTable),
	}
}

func (q *blobQ) Page(pageParams pgdb.OffsetPageParams) data.BlobRepo {
	q.stmt = pageParams.ApplyTo(q.stmt, "id")
	return q
}

func (q *blobQ) Clone() data.BlobRepo {
	return NewBlobRepo(q.db)
}

func (q *blobQ) Transaction(fn func(q data.BlobRepo) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *blobQ) Insert(createBlob *data.CreateBlob) (data.Blob, error) {
	stmt := sq.Insert(blobsTable).SetMap(map[string]interface{}{
		"body":     createBlob.Body,
		"owner_id": createBlob.OwnerID,
	}).Suffix("RETURNING *")

	var res data.Blob
	err := q.db.Get(&res, stmt)
	if err != nil {
		return res, err
	}
	return res, nil
}

func (q *blobQ) Get(id int64) (*data.Blob, error) {
	var res data.Blob

	err := q.db.Get(&res, q.stmt.Where(sq.Eq{"id": id}))
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &res, err
}

func (q *blobQ) Delete(id int64) (data.Blob, error) {
	var res data.Blob

	stmt := sq.Delete(blobsTable).Where(sq.Eq{"id": id}).
		Suffix("RETURNING *")

	err := q.db.Get(&res, stmt)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (q blobQ) Select() ([]data.Blob, error) {
	var res []data.Blob
	err := q.db.Select(&res, q.stmt)
	return res, err
}
