package postgres

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"testing"

	migr "github.com/rubenv/sql-migrate"
	"gitlab.com/Velnbur/blober/internal/assets"
	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/distributed_lab/kit/pgdb"
)

/* NOTE: Docker container with postgres can be started with:
docker run --name db-1 -e POSTGRES_DB=test \
-e POSTGRES_USER=test \
-e POSTGRES_PASSWORD=test \
-e POSTGRES_PORT=5432 -d -p 5432:5432 \
postgres \
-c ssl=on \
-c ssl_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem \
-c ssl_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
*/
const (
	dbName     = "test"
	dbUser     = "test"
	dbPassword = "test"
	dbPort     = 5432
)

var RepoBlob data.BlobRepo
var RepoOwner data.OwnerRepo
var ownerTest = data.Owner{
	PubKey:   "0000000000000000000000000000000000000000000000000000000000000000",
	Username: "init",
}

var testCases = []data.Blob{
	{
		ID:      1,
		Body:    json.RawMessage("{}"),
		OwnerID: ownerTest.PubKey,
	},
	{
		ID:      2,
		Body:    json.RawMessage("{\"test\": \"2\"}"),
		OwnerID: ownerTest.PubKey,
	},
	{
		ID:      3,
		Body:    json.RawMessage("{\"test\": \"3\"}"),
		OwnerID: ownerTest.PubKey,
	},
}

var migrations = &migr.EmbedFileSystemMigrationSource{
	FileSystem: assets.Migrations,
	Root:       "migrations",
}

// Insert tables and first values in them, also
// init an ownerTest struct that is needed for next tests
func migrate(db *pgdb.DB) {
	log.Print("Making migrations...")

	_, err := migr.Exec(db.RawDB(), "postgres", migrations, migr.Down)
	if err != nil {
		log.Fatalf("Migrate: %v", err)
	}
	_, err = migr.Exec(db.RawDB(), "postgres", migrations, migr.Up)
	if err != nil {
		log.Fatalf("Migrate: %v", err)
	}

	// Add intial values in each table
	// TODO: This a simpliest public key I could make,
	// but it actually must be 64 len byte array :)
	_, err = db.RawDB().Exec("INSERT INTO owners (pub_key, username) VALUES ($1, $2)",
		string(ownerTest.PubKey[:]),
		ownerTest.Username,
	)
	if err != nil {
		log.Fatalf("Insert owner: %v", err)
	}

	_, err = db.RawDB().Exec("INSERT INTO blobs (body, owner_id) VALUES ($1, $2)",
		testCases[0].Body,
		testCases[0].OwnerID,
	)
	if err != nil {
		log.Fatalf("Insert blob: %v", err)
	}
}

func TestMain(m *testing.M) {
	dbAddr := "localhost"
	if str := os.Getenv("TEST_DB_ADDR"); len(str) > 0 {
		dbAddr = str
	}
	dbUrl := fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
		dbUser, dbPassword, dbAddr, dbPort, dbName)

	db, err := pgdb.Open(pgdb.Opts{
		URL: dbUrl,
	})
	if err != nil {
		log.Fatalf("pgdb.Open: %v", err)
	}
	RepoBlob = NewBlobRepo(db)
	RepoOwner = NewOwnerRepo(db)

	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			log.Printf("Closing db: %v", err)
		}
	}(db.RawDB())

	// Craete tables and add initial values
	migrate(db)

	m.Run() // run all tests

	_, err = migr.Exec(db.RawDB(), "postgres", migrations, migr.Down)
	if err != nil {
		log.Fatalf("Migrate: %v", err)
	}
}

func TestOwnerGet(t *testing.T) {
	tests := []struct {
		name    string
		argID   string
		want    data.Owner
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:  "Get first one ownerTest",
			argID: ownerTest.PubKey,
			want:  ownerTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoOwner.Clone().Get(tt.argID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Owner.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Owner.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOwnerInsert(t *testing.T) {
	testValue1 := data.Owner{
		PubKey:   "2222222222222222222222222222222222222222222222222222222222222222",
		Username: "test",
	}
	tests := []struct {
		name     string
		argOwner data.Owner
		want     data.Owner
		wantErr  bool
	}{
		// TODO: Add test cases.
		{
			name:     "Insert Owner",
			argOwner: testValue1,
			want:     testValue1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoOwner.Clone().Insert(tt.argOwner)
			if (err != nil) != tt.wantErr {
				t.Errorf("Owner.Insert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Owner.Insert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobInsert(t *testing.T) {
	type args struct {
		createBlob *data.CreateBlob
	}
	tests := []struct {
		name    string
		args    args
		want    data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Test simple insert",
			args: args{
				createBlob: &data.CreateBlob{
					Body:    json.RawMessage("{\"test\":\"2\"}"),
					OwnerID: ownerTest.PubKey,
				},
			},
			want:    testCases[1],
			wantErr: false,
		},
		{
			name: "Test insert with more text",
			args: args{
				createBlob: &data.CreateBlob{
					Body:    json.RawMessage("{\"test\":\"3\"}"),
					OwnerID: ownerTest.PubKey,
				},
			},
			want:    testCases[2],
			wantErr: false,
		},
		{
			name: "Test insert with invalid OwnerID",
			args: args{
				createBlob: &data.CreateBlob{
					Body:    json.RawMessage("{}"),
					OwnerID: "",
				},
			},
			want:    data.Blob{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Insert(tt.args.createBlob)
			if (err != nil) != tt.wantErr {
				t.Errorf("blobQ.Insert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("blobQ.Insert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobGet(t *testing.T) {
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		args    args
		want    *data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Simple get",
			args: args{
				id: testCases[0].ID,
			},
			want: &testCases[0],
		},
		{
			name: "Get with more text",
			args: args{
				id: testCases[1].ID,
			},
			want: &testCases[1],
		},
		{
			name: "Already inserted blob",
			args: args{
				id: testCases[2].ID,
			},
			want: &testCases[2],
		},
		{
			name: "Get with invalid id",
			args: args{
				id: 0,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Get(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("blobQ.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("blobQ.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobSelect(t *testing.T) {
	tests := []struct {
		name    string
		want    []data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "Get all",
			want:    testCases,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Select()
			if (err != nil) != tt.wantErr {
				t.Errorf("blobQ.Select() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("blobQ.Select() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobPagination(t *testing.T) {
	type fields struct {
		limit, page uint64
		order       string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Get default",
			fields: fields{
				order: pgdb.OrderTypeAsc,
			},
			want: testCases,
		},
		{
			name: "Get first two",
			fields: fields{
				limit: 2,
				page:  0,
				order: pgdb.OrderTypeAsc,
			},
			want: testCases[:2],
		},
		{
			name: "Get last one",
			fields: fields{
				limit: 2,
				page:  1,
				order: pgdb.OrderTypeAsc,
			},
			want: testCases[2:],
		},
		{
			name: "Get nothing",
			fields: fields{
				limit: 2,
				page:  3,
				order: pgdb.OrderTypeAsc,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Page(pgdb.OffsetPageParams{
				Limit:      tt.fields.limit,
				PageNumber: tt.fields.page,
				Order:      tt.fields.order,
			}).Select()
			if (err != nil) != tt.wantErr {
				t.Errorf("Page().Select() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Page().Select() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobDelete(t *testing.T) {
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		args    args
		want    data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "delete 0 from testCases",
			args: args{
				id: testCases[0].ID,
			},
			want:    testCases[0],
			wantErr: false,
		},
		{
			name: "delete 1 from testCases",
			args: args{
				id: testCases[1].ID,
			},
			want:    testCases[1],
			wantErr: false,
		},
		{
			name: "delete 2 from testCases",
			args: args{
				id: testCases[2].ID,
			},
			want:    testCases[2],
			wantErr: false,
		},
		{
			name: "Get with invalid id",
			args: args{
				id: 0,
			},
			want:    data.Blob{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Delete(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("blobQ.Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("blobQ.Delete() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBlobGetDeleted(t *testing.T) {
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		args    args
		want    *data.Blob
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "get deleted 0 from testCases",
			args: args{
				id: testCases[0].ID,
			},
			want: nil,
		},
		{
			name: "get deleted 1 from testCases",
			args: args{
				id: testCases[1].ID,
			},
			want: nil,
		},
		{
			name: "get deleted 2 from testCases",
			args: args{
				id: testCases[2].ID,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := RepoBlob.Clone().Get(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("blobQ.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("blobQ.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
