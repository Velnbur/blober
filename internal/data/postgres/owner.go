package postgres

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/distributed_lab/kit/pgdb"
)

const ownerTable = "owners"

type ownerQ struct {
	db *pgdb.DB
}

func NewOwnerRepo(db *pgdb.DB) data.OwnerRepo {
	return &ownerQ{
		db: db.Clone(),
	}
}

func (q *ownerQ) Insert(owner data.Owner) (data.Owner, error) {
	var res data.Owner

	stmt := sq.Insert(ownerTable).SetMap(map[string]interface{}{
		"pub_key":  owner.PubKey,
		"username": owner.Username,
	}).Suffix("RETURNING *")

	err := q.db.Get(&res, stmt)
	return res, err
}

func (q *ownerQ) Clone() data.OwnerRepo {
	return NewOwnerRepo(q.db)
}

func (q *ownerQ) Get(pubKey string) (*data.Owner, error) {
	var res data.Owner

	stmt := sq.Select("*").
		From(ownerTable).
		Where(sq.Eq{"pub_key": pubKey})

	err := q.db.Get(&res, stmt)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return &res, err
}
