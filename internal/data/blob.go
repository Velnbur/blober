package data

import (
	"encoding/json"
	"gitlab.com/distributed_lab/kit/pgdb"

	"gitlab.com/Velnbur/blober/resources"
)

type BlobRepo interface {
	Clone() BlobRepo
	Insert(createBlob *CreateBlob) (Blob, error)
	Get(id int64) (*Blob, error)
	Page(pageParams pgdb.OffsetPageParams) BlobRepo
	Delete(id int64) (Blob, error)
	Transaction(fn func(q BlobRepo) error) error
	Select() ([]Blob, error)
}

type Blob struct {
	ID      int64           `db:"id"`
	Body    json.RawMessage `db:"body"`
	OwnerID string          `db:"owner_id"`
}

type CreateBlob struct {
	Body    json.RawMessage
	OwnerID string
}

func (blob *Blob) ToResource() resources.Blob {
	return resources.Blob{
		Key: resources.NewKeyInt64(blob.ID, resources.BLOB),
		Attributes: resources.BlobAttributes{
			Body: blob.Body,
		},
		Relationships: resources.BlobRelationships{
			Owner: resources.Relation{
				Data: &resources.Key{
					ID:   blob.OwnerID,
					Type: resources.OWNER,
				},
			},
		},
	}
}
