package data

import "gitlab.com/Velnbur/blober/resources"

type OwnerRepo interface {
	Clone() OwnerRepo
	Get(pubKey string) (*Owner, error)
	Insert(owner Owner) (Owner, error)
}

type Owner struct {
	Username string `db:"username"`
	PubKey   string `db:"pub_key"`
}

func (owner *Owner) ToResource() resources.Owner {
	return resources.Owner{
		Key: resources.Key{
			ID:   owner.PubKey,
			Type: resources.OWNER,
		},
		Attributes: resources.OwnerAttributes{
			Username: owner.Username,
		},
	}
}
