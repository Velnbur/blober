package requests

import (
	"errors"
	"github.com/go-chi/chi"
	"gitlab.com/Velnbur/blober/resources"
	"net/http"
	"strconv"
)

type BlobByIdRequest struct {
	IncludeOwner bool
	BlobID       int64
}

func NewBlobByIdRequest(r *http.Request) (BlobByIdRequest, error) {
	var req BlobByIdRequest
	req.IncludeOwner = r.URL.Query().Get("include") == string(resources.OWNER)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	req.BlobID = int64(id)
	if err != nil {
		return req, errors.New("invalid id")
	}
	return req, nil
}
