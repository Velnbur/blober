package requests

import (
	"encoding/json"
	"errors"
	"gitlab.com/Velnbur/blober/resources"
	"net/http"
)

type CreateOwnerRequest struct {
	Data resources.Owner
}

func NewCreateOwnerRequest(r *http.Request) (CreateOwnerRequest, error) {
	var req CreateOwnerRequest

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return req, err
	}

	return req, req.validate()
}

func (req *CreateOwnerRequest) validate() error {
	if req.Data.Type != resources.OWNER {
		return errors.New("invalid data type")
	}
	return nil
}
