package requests

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/Velnbur/blober/resources"
)

type CreateBlobRequest struct {
	Data         resources.CreateBlob
	IncludeOwner bool
}

func NewCreateBlobRequest(r *http.Request) (CreateBlobRequest, error) {
	var req CreateBlobRequest

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return req, err
	}

	if str := r.URL.Query().Get("include"); str == string(resources.OWNER) {
		req.IncludeOwner = true
	}

	return req, req.validate()
}

func (req *CreateBlobRequest) validate() error {
	if req.Data.Type != resources.CREATE_BLOB {
		return errors.New("invalid data type")
	}
	if req.Data.Relationships.Owner.Data.Type != resources.OWNER {
		return errors.New("invalid relationship data type")
	}
	return nil
}
