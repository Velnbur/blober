package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/Velnbur/blober/internal/data/postgres"
	"gitlab.com/Velnbur/blober/internal/service/handlers"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobRepo(postgres.NewBlobRepo(s.db)),
			handlers.CtxOwnerRepo(postgres.NewOwnerRepo(s.db)),
		),
	)
	r.Route("/", func(r chi.Router) {
		r.Route("/blobs", func(r chi.Router) {
			r.Post("/", handlers.CreateBlob)
			r.Get("/", handlers.GetBlobList)
		})
		r.Route("/blob/{id}", func(r chi.Router) {
			r.Get("/", handlers.GetBlobByID)
			r.Delete("/", handlers.DeleteBlobByID)
		})
		r.Route("/owner", func(r chi.Router) {
			r.Post("/", handlers.CreateOwner)
			r.Get("/{pub_key}", handlers.GetOwnerByPubKey)
		})
	})

	return r
}
