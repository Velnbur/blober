package handlers

import (
	"context"
	"net/http"

	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey       ctxKey = iota
	blobRepoCtxKey  ctxKey = iota
	ownerRepoCtxKey ctxKey = iota
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxBlobRepo(repo data.BlobRepo) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, blobRepoCtxKey, repo)
	}
}

func GetBlobRepo(r *http.Request) data.BlobRepo {
	return r.Context().Value(blobRepoCtxKey).(data.BlobRepo)
}

func CtxOwnerRepo(repo data.OwnerRepo) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, ownerRepoCtxKey, repo)
	}
}

func GetOwnerRepo(r *http.Request) data.OwnerRepo {
	return r.Context().Value(ownerRepoCtxKey).(data.OwnerRepo)
}
