package handlers

import (
	"net/http"

	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/Velnbur/blober/internal/service/requests"
	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewCreateBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	blob, err := GetBlobRepo(r).Insert(&data.CreateBlob{
		OwnerID: req.Data.Relationships.Owner.Data.ID,
		Body:    req.Data.Attributes.Body,
	})
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.BlobResponse{
		Data: blob.ToResource(),
	}
	if req.IncludeOwner {
		owner, err := GetOwnerRepo(r).Get(blob.OwnerID)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			return
		}
		ownerRes := owner.ToResource()
		response.Included.Add(&ownerRes)
	}

	Log(r).Infof("Blob id:%s created for owner id:%s",
		response.Data.ID,
		response.Data.Relationships.Owner.Data.ID,
	)
	ape.Render(w, response)
}
