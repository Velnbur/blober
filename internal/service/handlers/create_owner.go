package handlers

import (
	"gitlab.com/Velnbur/blober/internal/data"
	"gitlab.com/Velnbur/blober/internal/service/requests"
	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func CreateOwner(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewCreateOwnerRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	owner, err := GetOwnerRepo(r).Insert(data.Owner{
		PubKey:   req.Data.ID,
		Username: req.Data.Attributes.Username,
	})
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.OwnerResponse{
		Data: owner.ToResource(),
	}

	Log(r).Infof("Owner pub_key:%s created",
		response.Data.ID,
	)
	ape.Render(w, response)
}
