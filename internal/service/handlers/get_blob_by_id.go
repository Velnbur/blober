package handlers

import (
	"gitlab.com/Velnbur/blober/internal/service/requests"
	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func GetBlobByID(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewBlobByIdRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
	}

	blob, err := GetBlobRepo(r).Get(req.BlobID)
	if blob == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.BlobResponse{
		Data: blob.ToResource(),
	}

	if req.IncludeOwner {
		owner, err := GetOwnerRepo(r).Get(blob.OwnerID)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			return
		}
		resOwner := owner.ToResource()
		response.Included.Add(&resOwner)
	}
	ape.Render(w, response)
}
