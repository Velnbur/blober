package handlers

import (
	"gitlab.com/Velnbur/blober/internal/service/requests"
	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func DeleteBlobByID(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewBlobByIdRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blob, err := GetBlobRepo(r).Delete(req.BlobID)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	// blob wasn't found
	if blob.ID == 0 {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	response := resources.BlobResponse{
		Data: blob.ToResource(),
	}

	if req.IncludeOwner {
		owner, err := GetOwnerRepo(r).Get(blob.OwnerID)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			return
		}
		resOwner := owner.ToResource()
		response.Included.Add(&resOwner)
	}

	Log(r).Infof("Blob id:%s deleted for owner id:%s",
		response.Data.ID,
		response.Data.Relationships.Owner.Data.ID,
	)
	ape.Render(w, response)
}
