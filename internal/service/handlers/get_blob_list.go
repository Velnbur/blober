package handlers

import (
	"gitlab.com/distributed_lab/kit/pgdb"
	"net/http"
	"net/url"
	"strconv"

	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetBlobList(w http.ResponseWriter, r *http.Request) {
	includeOwner := r.URL.Query().Get("include") == string(resources.OWNER)

	pageParams := getUrlParams(r)
	blobs, err := GetBlobRepo(r).Page(pageParams).Select()
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ownerRepo := GetOwnerRepo(r)

	var response resources.BlobListResponse

	for _, blob := range blobs {
		blobRes := blob.ToResource()
		response.Data = append(response.Data, blobRes)

		if includeOwner {
			owner, err := ownerRepo.Get(blob.OwnerID)
			if err != nil {
				ape.RenderErr(w, problems.InternalError())
				return
			}
			ownerRes := owner.ToResource()
			response.Included.Add(&ownerRes)
		}
	}

	response.Links = createLinks(r.URL, pageParams)
	ape.Render(w, response)
}

const (
	pageLimitParam  = "page[limit]"
	pageNumberParam = "page[number]"
	pageOrderParam  = "page[order]"
)

func getUrlParams(r *http.Request) pgdb.OffsetPageParams {
	var params pgdb.OffsetPageParams
	query := r.URL.Query()

	if str := query.Get(pageLimitParam); len(str) != 0 {
		pageLimit, err := strconv.Atoi(str)
		if err != nil {
			params.Limit = uint64(pageLimit)
		}
	}
	if str := query.Get(pageNumberParam); len(str) != 0 {
		pageNumber, err := strconv.Atoi(str)
		if err != nil {
			params.PageNumber = uint64(pageNumber)
		}
	}
	if str := query.Get(pageOrderParam); len(str) != 0 {
		switch str {
		case pgdb.OrderTypeAsc, pgdb.OrderTypeDesc:
			params.Order = str
		}
	}

	return params
}

func createLinks(url *url.URL, pageParams pgdb.OffsetPageParams) *resources.Links {
	links := &resources.Links{
		Self: createLinkStr(url, pageParams.Limit, pageParams.PageNumber, pageParams.Order),
		Next: createLinkStr(url, pageParams.Limit, pageParams.PageNumber+1, pageParams.Order),
	}

	if pageParams.PageNumber > 1 {
		links.Prev = createLinkStr(url, pageParams.Limit, pageParams.PageNumber+1, pageParams.Order)
	}
	return links
}

func createLinkStr(url *url.URL, limit, page uint64, sort string) string {
	query := url.Query()
	query.Set(pageLimitParam, strconv.Itoa(int(limit)))
	query.Set(pageNumberParam, strconv.Itoa(int(page)))
	query.Set(pageOrderParam, sort)

	url.RawQuery = query.Encode()
	return url.String()
}
