package handlers

import (
	"github.com/go-chi/chi"
	"gitlab.com/Velnbur/blober/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func GetOwnerByPubKey(w http.ResponseWriter, r *http.Request) {
	pubKey := chi.URLParam(r, "pub_key")
	owner, err := GetOwnerRepo(r).Get(pubKey)
	if owner == nil {
		ape.RenderErr(w, problems.NotFound())
	}
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
	}

	ape.Render(w, resources.OwnerResponse{
		Data: owner.ToResource(),
	})
}
