-- +migrate Up

CREATE TABLE owners (
       pub_key VARCHAR(64) PRIMARY KEY,
       username VARCHAR(64) NOT NULL
);

CREATE TABLE blobs (
       id BIGSERIAL PRIMARY KEY,
       body JSONB DEFAULT '{}'::JSONB,
       owner_id VARCHAR(64) NOT NULL REFERENCES owners (pub_key) ON DELETE CASCADE
);

-- +migrate Down

DROP TABLE blobs;
DROP TABLE owners;
