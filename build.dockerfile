FROM golang:1.17-alpine as buildbase

RUN apk add git build-base
WORKDIR /go/src/gitlab.com/Velnbur/blober
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/blober gitlab.com/Velnbur/blober


FROM alpine:3.9

RUN apk add --no-cache ca-certificates
COPY config.yaml /etc/blober.yaml
COPY --from=buildbase /usr/local/bin/blober /usr/local/bin/blober

ENV KV_VIPER_FILE=/etc/blober.yaml

ENTRYPOINT ["blober", "run", "service"]
