# Blober

A simple [JsonAPI](https://jsonapi.org/) service for managing (creating, deleting,
listing) `blobs` - an arbitrary json data.

Documentation written by [openapi](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.0.md#format)
specification, so you can read in `docs` dir anything you
want to know about API.
