package main

import (
	"os"

	"gitlab.com/Velnbur/blober/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
