/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type CreateBlobAttributes struct {
	Body json.RawMessage `json:"body"`
}
