/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	BLOB        ResourceType = "blob"
	CREATE_BLOB ResourceType = "create_blob"
	OWNER       ResourceType = "owner"
)
